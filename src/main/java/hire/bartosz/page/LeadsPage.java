package hire.bartosz.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.testng.Assert.assertEquals;

/**
 * Created by Bartosz on 2015-07-13.
 */
public class LeadsPage extends BasePage {

    private String LEAD_LINK_XPATH = "//h3/a[text()='%s']";

    @FindBy(id = "leads-new")
    private WebElement addNewLeadButton;

    @FindBy(id = "lead-first-name")
    private WebElement firstNameInput;

    @FindBy(id = "lead-last-name")
    private WebElement lastNameInput;

    @FindBy(id = "lead-company-name")
    private WebElement companyNameInput;

    @FindBy(css = "div#container button")
    private WebElement saveButton;

    @FindBy(css = ".lead-status")
    private WebElement leadStatus;

    @FindBy(css = ".list-head input[type = text]")
    private WebElement filterInput;

    @FindBy(css = ".btn.clear-all-filters")
    private WebElement clearFilterButton;

    public LeadsPage(WebDriver driver) {
        super(driver);
    }

    public LeadsPage clickAddNew(){
        addNewLeadButton.click();
        return this;
    }

    public LeadsPage fillFirstName(String firstName){
        firstNameInput.clear();
        firstNameInput.sendKeys(firstName);
        return this;
    }

    public LeadsPage fillLastName(String lastName){
        lastNameInput.clear();
        lastNameInput.sendKeys(lastName);
        return this;
    }

    public LeadsPage fillCompanyName(String companyName){
        companyNameInput.clear();
        companyNameInput.sendKeys(companyName);
        return this;
    }

    public LeadsPage save(){
        saveButton.click();
        return this;
    }

    public LeadsPage filterLeads(String leadName) {
        filterInput.sendKeys(leadName);
        waitForElement(clearFilterButton);
        return this;
    }

    public LeadsPage selectLead(String leadName) {
        getElement(By.xpath(String.format(LEAD_LINK_XPATH, leadName))).click();
        return this;
    }

    public LeadsPage assertStatusIs(String expectedStatus){
        assertEquals(leadStatus.getText(), expectedStatus);
        return this;
    }
}
