package hire.bartosz.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Bartosz on 2015-07-13.
 */
public class SettingsPage extends BasePage {

    @FindBy(xpath = "//div[@id='sidebar']//a[text()='Leads']")
    private WebElement leadsSectionLink;

    @FindBy(css = "ul[id = 'leads-settings-tabs'] a[data-toggle = 'lead-status']")
    private WebElement leadStatusesTabLink;

    @FindBy(xpath = "//div[@class = 'item form'][not(@style)]//button[text() = 'Save']")
    private WebElement saveButton;

    private String EDIT_BUTTON_XPATH = "//h4[text() = '%s']/ancestor::span[@class = 'named-object-lead']//button[text() = 'Edit']";
    private String INPUT_XPATH = "//input[@value = '%s']";

    public SettingsPage(WebDriver driver) {
        super(driver);
    }

    public SettingsPage goToLeadsSection(){
        leadsSectionLink.click();
        return this;
    }

    public SettingsPage goToLeadStatusesTab(){
        leadStatusesTabLink.click();
        return this;
    }

    public SettingsPage clickEditNameOnStatus(String statusName){
        getElement(By.xpath(String.format(EDIT_BUTTON_XPATH, statusName))).click();
        return this;
    }

    public SettingsPage fillStatusName(String statusName, String newStatusName) {
        WebElement input = getElement(By.xpath(String.format(INPUT_XPATH, statusName)));
        input.clear();
        input.sendKeys(newStatusName);
        return this;
    }

    public SettingsPage save() {
        saveButton.click();
        return this;
    }
}
