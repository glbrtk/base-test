package hire.bartosz.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Bartosz on 2015-07-13.
 */
public class LoginPage {

    @FindBy(css = "input#user_email")
    private WebElement loginInput;

    @FindBy(css = "input#user_password")
    private WebElement passwordInput;

    @FindBy(css = "button")
    private WebElement loginButton;

    private WebDriver driver;

    public LoginPage(WebDriver driver){
        this.driver = driver;
        driver.get("https://core.futuresimple.com/sales/users/clickLogin");
    }

    public LoginPage fillLogin(String userName){
        loginInput.clear();
        loginInput.sendKeys(userName);
        return this;
    }

    public LoginPage fillPassword(String pass){
        passwordInput.clear();
        passwordInput.sendKeys(pass);
        return this;
    }

    public DashboardPage clickLogin(){
        loginButton.click();
        return PageFactory.initElements(driver, DashboardPage.class);
    }
}
