package hire.bartosz.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Bartosz on 2015-07-13.
 */
public abstract class BasePage {

    @FindBy(id = "nav-leads")
    private WebElement leadsNavigationButton;

    @FindBy(css = "li[id='user-dd'] i.base-icon-arrow-down")
    private WebElement dropDownArrow;

    @FindBy(xpath = "//ul[contains(@class, 'dropdown-menu')]//strong[text()='Settings']")
    private WebElement settingsOption;

    private WebDriver driver;

    public BasePage(WebDriver driver){
        this.driver = driver;
    }

    public LeadsPage goToLeads(){
        leadsNavigationButton.click();
        return PageFactory.initElements(driver, LeadsPage.class);
    }

    public SettingsPage goToSettings(){
        dropDownArrow.click();
        settingsOption.click();
        return PageFactory.initElements(driver, SettingsPage.class);
    }

    public WebElement getElement(By by){
        return driver.findElement(by);
    }

    public void waitForElement(WebElement element) {
        new WebDriverWait(driver, 30)
                .until(ExpectedConditions.elementToBeClickable(element));
    }
}
