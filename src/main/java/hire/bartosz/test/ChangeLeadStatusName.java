package hire.bartosz.test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import hire.bartosz.page.LoginPage;

import java.util.concurrent.TimeUnit;

/**
 * Created by Bartosz on 2015-07-13.
 */

public class ChangeLeadStatusName {

    private WebDriver driver;
    private static final String OLD_STATUS_NAME = "New";
    private static final String NEW_STATUS_NAME = "something different";
    private static final String LEAD_FIRST_NAME = "Ronnie";
    private static final String LEAD_LAST_NAME = "Coleman";
    private static final String LEAD_COMPANY_NAME = "Base";

    @BeforeClass
    public void initialize(){
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void changeLeadStatusName(){
        PageFactory.initElements(driver, LoginPage.class)
                .fillLogin("gula.bartosz+base@gmail.com")
                .fillPassword(", who is g8 automation specialist ;)")
                .clickLogin()
                .goToLeads()
                .clickAddNew()
                .fillFirstName(LEAD_FIRST_NAME)
                .fillLastName(LEAD_LAST_NAME)
                .fillCompanyName(LEAD_COMPANY_NAME)
                .save()
                    .assertStatusIs(OLD_STATUS_NAME)
                .goToSettings()
                .goToLeadsSection()
                .goToLeadStatusesTab()
                .clickEditNameOnStatus(OLD_STATUS_NAME)
                .fillStatusName(OLD_STATUS_NAME, NEW_STATUS_NAME)
                .save()
                .goToLeads()
                .filterLeads(LEAD_FIRST_NAME + " " + LEAD_LAST_NAME)
                .selectLead(LEAD_FIRST_NAME + " " + LEAD_LAST_NAME)
                    .assertStatusIs(NEW_STATUS_NAME);
    }

    @AfterTest
    public void cleanup(){
        driver.quit();
    }
}
